# Boilerplate Electron React Typescript

This is a boilerplate project for an Electron app built with React with TypeScript (strict), eslint, and other things.

# Copying
- Replace these strings:
  - `boilerplate-electron-react-typescript`
  - `boilerplate_electron_react_typescript`
  - `Application description`
  - `https://gitlab.com/7thias` (package.json author)
  - `MIT` (package.json license)

# TODO
- use JS eslint config file, single quotes.
- use typescript-eslint, react-hooks, react-recommended. Tailor rules. array-type, comma-dangle.
- add editorconfig file, readme note.
- try to use pnpm.